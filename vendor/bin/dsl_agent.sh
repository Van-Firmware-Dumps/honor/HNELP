# support execute cloud sh or bin only in beta
function dsl_agent_main()
{
	usertype=`getprop ro.logsystem.usertype`
	if [ $usertype != 3 ];then
		exit 1
	fi

	case $1 in
		"dsl_sh")
			sh_file='/data/log/dslscript/dsl_agent/dsl_sh_main.sh'
			dos2unix $sh_file
			runcon u:r:su:s0 /system/bin/sh $sh_file
			;;
		"pdr2ssr")
			pdr_cfg_file='/data/log/dslscript/dsl_pdr2ssr/dsl_pdr2ssr.conf'
			runcon u:r:su:s0 /vendor/bin/pdr_dump_cfg $pdr_cfg_file
			;;
		*)
			exit 1
			#runcon u:r:su:s0 $bin_file
			;;
	esac
	exit 0
}

dsl_agent_main $1
